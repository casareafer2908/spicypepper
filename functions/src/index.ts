import * as functions from 'firebase-functions';
import {dialogflow} from "actions-on-google";
import * as admin from 'firebase-admin';

/***Initializations and variables***/

admin.initializeApp();
const conversation = dialogflow({debug: true});

/***Exports***/
export const dialogflowFirebaseFulfillment = functions.https.onRequest((conversation));


/**
 * Funcion que comienza un juego nuevo.
 */
export const startGame = functions.https.onRequest(async (request, response) => {
    try {
        let session = request.body.session;
        await generateGame(session);

        response.status(200).json({
            message: `success`
        });
    } catch (e) {
        console.log(e);
        response.status(500).json({
            message: `error`
        });
    }

    dialogflowFirebaseFulfillment
});

/**
 * Generate a new game in the provided session
 * @param session session number
 */
async function generateGame(session: string): Promise<string> {
    const x = randomNum();
    console.log(`The number to guess is: ${x}`);
    let refGame = admin.firestore().collection("games").doc();
    await refGame.set({gameId: refGame.id, number: x});
    return refGame.id
}

/***Intent Handlers***/

conversation.intent('Default Welcome Intent - yes', async (conv) => {
    try {
        conv.ask(`Great. So please guess a number between 1 and 100 in 5 tries`);
        // @ts-ignore
        let gameId = await generateGame(conv.body.session);
        conv.contexts.set("game_id", 5, {id: gameId});
        console.log(`The game ${gameId} was created`);
    }
    catch (e) {
        console.log(`there was an error adding an random number into firebase from dialogflow game: ` + e)
    }
});

conversation.intent('Default Welcome Intent', conv => {
    conv.ask(`Hello! this is the Guess Number - a - Palooza, to see our funny cat pictures please look into
     your assistant app in your mobile device, If you want to play say Number-a-Palooza, if you want to exit you can 
     say Cancel or Exit at any time! so, Do you want to play?`);
});


conversation.intent<{ number: number }>('sayNumber', (conv, {number}) => {
    conv.ask(`The game ${conv.contexts.get("gameid")} was created`);
    console.log(`The game ${conv.contexts.get("gameid")} was created`);
    console.log(`thing`)
});


/**
 * Return a random number between 1 to 100
 */
function randomNum(): number {
    return Math.floor(Math.random() * 100) + 1;
}